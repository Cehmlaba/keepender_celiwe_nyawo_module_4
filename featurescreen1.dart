import 'package:flutter/material.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.green,
          scaffoldBackgroundColor: Colors.cyanAccent),
      title: "WELCOME TO MY APP",
      home: Scaffold(
          appBar: AppBar(
              title: const Text(
        'WELCOME TO MY APP',
        style: TextStyle(color: Colors.white),
      ))),
    );
  }
}
