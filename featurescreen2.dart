import 'package:flutter/material.dart';

class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            primarySwatch: Colors.green,
            scaffoldBackgroundColor: Colors.cyanAccent),
        title: "please share our link",
        home: Scaffold(
          appBar: AppBar(
              title: const Text(
            'PLEASE SHARE OUR LINK',
            style: TextStyle(color: Colors.white),
          )),
        ));
  }
}
