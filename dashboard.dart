import 'package:flutter/material.dart';
import 'package:module3/featurescreen1.dart';
import 'package:module3/featurescreen2.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Dashboard',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Column(children: <Widget>[
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const FeatureScreen1()),
                  )
                },
                child: const Text("Feature Screen 1"),
              )),
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const FeatureScreen2()),
                  )
                },
                child: const Text("Feature Screen 2"),
              ))
        ]));
  }
}
